import { svelte } from '@sveltejs/vite-plugin-svelte';
import { resolve } from 'path';
import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svelte(),
    dts({
      outputDir: resolve(__dirname, 'dist/4d-algo'),
      include: [resolve(__dirname, 'lib/4d-algo-src/**/*.ts')],
      insertTypesEntry: true,
      rollupTypes: true,
    })
  ],
  build: {
    lib: {
      // Could also be a dictionary or array of multiple entry points
      entry: resolve(__dirname, 'lib/4d-algo-src/index.ts'),
      name: '4DAlgo',
      // the proper extensions will be added
      fileName: '4d-algo/index',
    },
    // Disable until I have a need.  Its contaminating the lib dist
    copyPublicDir: false,
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: [],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        // globals: {
        //   vue: 'Vue'
        // }
      }
    }
  }
})
