import { BinarySearchTree } from "./bst/tree";
import { replayToGenerateStateSeries } from "./replay";
import type { StateSnapshot, Transaction } from "./types";

export class AlgoTimeModel {

    private tree = new BinarySearchTree<StateSnapshot>();

    constructor(private txns: Transaction[]) {}

    async build(): Promise<AlgoTimeModel> {
        const stateSeries = replayToGenerateStateSeries(this.txns);

        console.time(`buildTree:${stateSeries.length}`);

        this.tree.buildFromOrderedData(stateSeries, (s) => s.roundTime);

        console.timeEnd(`buildTree:${stateSeries.length}`);

        return this;
    }

    async find(maxTimeSecs: number) {
        return this.tree.find(maxTimeSecs);
    }
}