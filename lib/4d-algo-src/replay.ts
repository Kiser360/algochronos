import type { StateSnapshot, Transaction } from "./types";

export function replayToGenerateStateSeries(txns: Transaction[]) {

    console.time(`replayTxns:${txns.length}`);

    // ASC, default behaviour for comparing arrays is to compare each entry sequentially.
    //  - If first key is identical it will defer to the second key etc...
    //  - This produces multi-tiered sorting, first by round-time second by the offset when round-time is identical
    const orderedTxns = txns.sort((a, b) => [
        a["round-time"] - b["round-time"],
        a["intra-round-offset"] - b["intra-round-offset"]
    ] as any);

    const startingState = {};
    const stateSnapshots: StateSnapshot[] = orderedTxns.map(tx => {
        if (!tx["global-state-delta"]) {
            return null;
        }
        tx["global-state-delta"].forEach(change => {
            startingState[change.key] = change.value;
        });

        return {
            roundTime: tx["round-time"],
            tx,
            state: Object.assign({}, startingState),
        }
    }).filter(Boolean);

    const result = squashByRoundTime(stateSnapshots);
    console.log(`${txns.length} -> ${stateSnapshots.length} -> ${result.length}`);
    console.timeEnd(`replayTxns:${txns.length}`);

    return result;
}

function squashByRoundTime(orderedSnapshots: StateSnapshot[]) {
    const squashedSnapshots: StateSnapshot[] = [];
    let currentRoundTime = 0;
    let pendingSnapshot = null;

    // We know the order of snapshots, so we can iterate every snapshot only adding to the result
    //  when reaching the edge of a roundTime.  The order ensures the snapshot just before the edge (n-1),
    //  will always be the latest snapshot for that round.
    orderedSnapshots.forEach(snapshot => {
        if (snapshot.roundTime !== currentRoundTime && pendingSnapshot) {
            squashedSnapshots.push(pendingSnapshot);
        }

        pendingSnapshot = snapshot;
        currentRoundTime = snapshot.roundTime;
    });

    // There will always be a leftover
    if (pendingSnapshot) {
        squashedSnapshots.push(pendingSnapshot);
    }

    return squashedSnapshots;
}