export interface Transaction {
    'application-transaction': any;
    'close-rewards': number,
    'closing-amount': number,
    'confirmed-round': number,
    'created-application-index'?: number,
    'fee': number,
    'first-valid': number,
    'genesis-hash': string,
    'genesis-id': string,
    'global-state-delta': StateDelta[],
    'id': string,
    'intra-round-offset': number,
    'last-valud': number,
    'note': string, //b64
    'receiver-rewards': number,
    'round-time': number,
    'sender': string,
    'sender-rewards': number,
    'signature': Signature,
    'tx-type': 'appl' | string,
}

export interface StateDelta {
    key: string, //b64
    value: {
        action: 1 | 2,
        bytes?: string, //b64
        uint: number,
    }
}

export type Signature = { sig: string }

export interface StateSnapshot {
    roundTime: number,
    tx: Transaction,
    state: Record<string, StateDelta>,
}
