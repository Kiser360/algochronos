
export const atob = globalThis.atob || ((src: string) => {
    return Buffer.from(src, 'base64').toString('binary');
})

export const btoa = globalThis.btoa || ((src: string) => {
    return Buffer.from(src, 'binary').toString('base64');
})

export function b64ToUint8(b64: string): Uint8Array {
    return Uint8Array.from(atob(b64), c => c.charCodeAt(0));
}

export function textToUint8(text: string): Uint8Array {
    return Uint8Array.from(text, c => c.charCodeAt(0));
}

export function uint8ToB64(bytes: Uint8Array): string {
    return btoa(uint8ToBinaryString(bytes));
}

export function uint8ToBinaryString(bytes: Uint8Array): string {
    return Array.from(bytes).map(b => String.fromCharCode(b)).join('');
}


export function batch<T = any>(src: T[], batchLength: number): T[][] {
    const hasRemainder = src.length % batchLength !== 0;
    return src.reduce((acc, curr, i) => {
        acc[Math.floor(i / batchLength)].push(curr);
        return acc;
    }, Array.from({
        length: Math.floor(src.length / batchLength) + (hasRemainder ? 1 : 0)
    }).map(() => [] as T[]));
}

export function tick(time: number) {
    return new Promise(res => setTimeout(res, time));
}