export class Defered<T = any> {

    public resolve: (val: unknown) => void;
    public reject: (error: unknown) => void;
    public promise: Promise<T>;

    constructor() {
        this.promise = new Promise((res, rej) => {
            this.resolve = res as any;
            this.reject = rej;
        });
    }
}