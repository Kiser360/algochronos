import { backOff, type IBackOffOptions } from 'exponential-backoff';
import { Defered } from './defered';
import type { Transaction } from './types';

export class AlgoTimeIndexerClient {

    private baseHeaders: Record<string, string>;
    private baseUrl: string;

    constructor(
        public url: string,
        public port: number = 443,
        token: string = '',
    ) {
        this.baseHeaders = {
            'X-Indexer-API-Token': token,
        };

        if (!(url.startsWith('http') || url.startsWith('https'))) {
            this.url = `https://${url}`;
        }

        if (this.url.endsWith('/')) {
            this.url = this.url.slice(0, -1);
        }

        this.baseUrl = `${this.url}:${port}`;
    }

    async getAllAppTxns(appId: number, maxTxns = Infinity, onProgress: (num: number) => void = () => {}): Promise<Transaction[]> {
        
        // Use defered passthrough instead of relying on recursive stack returns,
        //   enables more recursive calls before stack/memory errors
        const defered = new Defered();
        this.recurseForTxns(appId, maxTxns, defered, onProgress);
        return defered.promise;
    }

    private async recurseForTxns(appId: number, maxTxns: number, defered: Defered, onProgress: (num: number) => void, next: string = '', txns: any[] = []) {
        onProgress(txns.length);
        const url = `${this.baseUrl}/v2/transactions?limit=${1000}&next=${next}&tx-type=appl&application-id=${appId}`;

        const backoffOptions: Partial<IBackOffOptions> = { delayFirstAttempt: true };
        const backoffAction = () => fetch(url, { headers: this.baseHeaders }).then(res => {
            if (res.status >= 300) {
                console.error('Retrying...', res);
                throw new Error(`Unexpected Server Response: ${res.status}`);
            }
            return res.json();
        });
        
        try {
            const json = await backOff(backoffAction, backoffOptions);
            const nextTxns = txns.concat(json.transactions);
            const nextNext = json['next-token'];
    
            if(!nextNext || json.transactions.length === 0 || nextTxns.length >= maxTxns) {
                defered.resolve(nextTxns);
            } else {
                this.recurseForTxns(appId, maxTxns, defered, onProgress, nextNext, nextTxns);
            }
        } catch(e) {
            defered.reject(e);
        }
    }
}