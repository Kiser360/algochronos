import { Defered } from '../defered';
import { Node } from './node';

export class BinarySearchTree<T> {
    root: Node<T> | null = null;

    async buildFromOrderedData(data: T[], keySelector: (t: T) => number) {
        if (this.root !== null) {
            throw new Error('Must build from scratch');
        }

        this.root = this.buildSubtreeFromOrderedData(data, keySelector, 0, data.length - 1, null);
    }

    private buildSubtreeFromOrderedData(
        allData: T[],
        keySelector: (t: T) => number,
        start: number,
        end: number,
        parent: Node<T> | null,
    ) {
        if (start > end) return null;

        const mid = Math.floor((start + end) / 2);
        const rootData = allData[mid];
        const node = new Node(keySelector(rootData), rootData);

        node.parent = parent;
        node.left = this.buildSubtreeFromOrderedData(allData, keySelector, start, mid - 1, node);
        node.right = this.buildSubtreeFromOrderedData(allData, keySelector, mid + 1, end, node);

        return node;
    }

    /**
   * Finds the closest node with a key that is up-to but not exceeding the provided key to search for
   *
   *
   * @param maxKey - search for keyed data that is up-to but not exceeding this provided key
   * @returns the data value of the node that was found, it can be null
   *
   * @beta
   */
    find(maxKey: number): Promise<T | null> {
        const defered = new Defered<T | null>();
        const onFound = (t: Node<T> | null) => {
            // console.log(`!! : ${t.key}`, t);

            defered.resolve(t?.data);
        };

        this.findNode(maxKey, this.root, onFound);

        return defered.promise;
    }

    private findNode(maxKey: number, node: Node<T>, onFound: (node: Node<T> | null) => void) {
        if (node === null) { // Tree has no root
            onFound(null);
            return;
        }

        if (node.key === maxKey) {
            // console.log('Exact match');
            onFound(node);
            return;
        }

        if (node.key > maxKey) {
            if (node.left === null) {
                // console.log('Leaf, cant go lower');
                onFound(node.parent);
                return;
            } else {
                // console.log(`<- : ${node.key}`);
                this.findNode(maxKey, node.left, onFound);
                return;
            }
        }

        if (node.key < maxKey) {
            if (node.right === null) {
                // console.log('Leaf, cant go higher');
                onFound(node);
                return;
            } else {
                // console.log(`-> : ${node.key}`)
                this.findNode(maxKey, node.right, onFound);
                return;
            }
        }

    }
}