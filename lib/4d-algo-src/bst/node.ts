export class Node<T> {

    parent: Node<T> | null = null;
    left: Node<T> | null = null;
    right: Node<T> | null = null;

    constructor(
        public key: number,
        public data: T
    ) { }
}