import './app.css'
import App from './App.svelte'
import { AlgoTimeIndexerClient } from '../lib/4d-algo-src';
import { AlgoTimeModel } from '../lib/4d-algo-src/model';

const app = new App({
  target: document.getElementById('app')
})

const client = new AlgoTimeIndexerClient('https://algoindexer.algoexplorerapi.io/', 443);
client.getAllAppTxns(705663269, 1, console.log)
  .then(txns => {
    const model = new AlgoTimeModel(txns);
    console.log(txns);
    return model.build();
  })
  .then(model => {
    (window as any)._find = (time: number) => model.find(time);
  })
  .catch(console.error);

export default app
